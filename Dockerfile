FROM ubuntu/nginx:1.18-22.04_beta

LABEL maintainer="Kyaw Sithu"

USER root
ARG WWWGROUP
ENV TZ=UTC

RUN apt update
RUN apt install lsb-release ca-certificates apt-transport-https software-properties-common -y
RUN add-apt-repository ppa:ondrej/php
# RUN add-apt-repository ppa:ondrej/nginx
# RUN apt-get install -y nginx
RUN apt-get install -y php8.1-fpm
RUN apt-get install -y php-common php-mysql php-cgi php-mbstring php-curl php-gd php-xml php-xmlrpc php-pear

# RUN echo "\ndaemon off;" >> /etc/nginx/nginx.conf
# RUN sed -i -e "s/;\?daemonize\s*=\s*yes/daemonize = no/g" /etc/php/8.1/fpm/php-fpm.conf

# Nginx config
# RUN rm /etc/nginx/sites-enabled/default

# PHP config
# RUN sed -i -e "s/;\?date.timezone\s*=\s*.*/date.timezone = Europe\/Kiev/g" /etc/php/8.1/fpm/php.ini

# COPY --from=php:8.1.2-fpm /usr/local/bin/docker-php-ext-install /usr/local/bin/docker-php-ext-install
# COPY --from=php:8.1.2-fpm /usr/local/bin/docker-php-source /usr/local/bin/docker-php-source
# COPY --from=php:8.1.2-fpm /usr/local/bin/docker-php-ext-enable /usr/local/bin/docker-php-ext-enable
# COPY --from=php:8.1.2-fpm /usr/local/bin/docker-php-ext-configure /usr/local/bin/docker-php-ext-configure
# COPY --from=php:8.1.2-fpm /usr/local/bin/phpize /usr/local/bin/phpize
# COPY --from=php:8.1.2-fpm /usr/src/php.tar.xz /usr/src/php.tar.xz

# RUN cd /usr/local/lib && mkdir php
# RUN cp -R /usr/lib/php/20210902/build /usr/local/lib/php/build
# RUN chmod +x /usr/local/lib/php/build